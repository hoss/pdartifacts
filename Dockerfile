# syntax = docker/dockerfile:experimental

FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /opt/pdartifacts
COPY requirements.txt /opt/pdartifacts
RUN pip install -r requirements.txt
COPY . /opt/pdartifacts/
